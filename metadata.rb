name 'packer'
maintainer 'Julien Levasseur'
maintainer_email 'contact@julienlevasseur.ca'
license 'All Rights Reserved'
description 'Installs/Configures packer'
long_description 'Installs/Configures packer'
version '0.1.0'
chef_version '>= 13.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://gitlab.com/julienlevasseur/chef-packer/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://gitlab.com/julienlevasseur/chef-packer'

depends 'zipfile'