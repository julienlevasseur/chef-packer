#
# Cookbook:: packer
# Recipe:: default
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

directory node['packer']['install_dir'] do
  recursive true
end

remote_file "#{node['packer']['install_dir']}/packer.zip" do
  source "https://releases.hashicorp.com/packer/#{node['packer']['version']}/packer_#{node['packer']['version']}_linux_amd64.zip"
  mode '0755'
  not_if { ::File.exist?("#{node['packer']['install_dir']}/packer.zip") }
  not_if { ::File.exist?("#{node['packer']['install_dir']}/packer") }
end

zipfile "#{node['packer']['install_dir']}/packer.zip" do
  into "#{node['packer']['install_dir']}/"
  only_if { ::File.exist?("#{node['packer']['install_dir']}/packer.zip") }
end

file "#{node['packer']['install_dir']}/packer.zip" do
  action :delete
end
